# -*- coding: utf-8 -*-
__author__ = 'luisfernando'

from django import forms
from .models import *


class CategoryForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                           max_length=128, label="Category name")
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    start_page = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': 'make-switch', 'data-on-text': u'Sí',
                                                                      'data-off-text': 'No',}), initial=True, required=False)

    class Meta:
        model = Category


class PageForm(forms.ModelForm):
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label='Title', max_length=128)
    url = forms.URLField(widget=forms.URLInput(attrs={'class': 'form-control'}), label='URL')
    views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    class Meta:
        model = Page

        fields = ('title', 'url', 'views')

    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        if url and not (url.startswith('http://') or url.startswith('https://')):
            url = 'http://' + url
            cleaned_data['url'] = url

        return cleaned_data


class UserForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="username")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="email address")

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class UserProfileForm(forms.ModelForm):
    website = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="website")

    class Meta:
        model = UserProfile
        fields = ('website', 'picture')


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="username")
    password = forms.CharField(label="password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))



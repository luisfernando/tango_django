from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
import django.views.static
#from .views import index, about, category, add_page, add_category, register, user_login
from .views import *
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rango.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', index, name="index"),
    url(r'^about/$', about, name="about"),
    url(r'^category/(?P<category_name_url>\w+)$', category, name="category"),
    url(r'^category/(?P<category_name_url>\w+)/add_page/$', add_page, name="add_page"),
    url(r'^add_category/$', add_category, name="add_category"),
    url(r'^visit_page/(?P<page>\w+)$', visit_page, name="visit_page"),
    url(r'^register/$', register, name="register"),
    url(r'^login/$', user_login, name="login"),
    url(r'^logout/$', user_logout, name="logout"),
    url(r'^test_login/$', test_login, name="test_login"),
    url(r'^admin/', include(admin.site.urls), name="admin"),
)


if settings.DEBUG:
    urlpatterns += patterns('django.views.static',
                            (r'media/(?P<path>.*)',
                             'serve',
                             {'document_root': settings.MEDIA_ROOT}
                            )
    )
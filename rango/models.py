__author__ = 'luisfernando'

from django.db import models
from django.contrib.auth.models import User
import datetime


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'categories'

    name = models.CharField(max_length=128, unique=True)
    start_page = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

    @property
    def url(self):
        return self.name.replace(' ', '_')

    @staticmethod
    def url_decode(name):
        return name.replace('_', ' ')

    @staticmethod
    def url_encode(name):
        return name.replace(' ', '_')


class Page(models.Model):
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=128)
    url = models.URLField()
    views = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def __unicode__(self):
        return self.title


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __unicode__(self):
        return self.user.username
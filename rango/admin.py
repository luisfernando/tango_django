from django.contrib import admin
from .models import *


class PageAdmin(admin.ModelAdmin):
    fields = ['title', 'url', 'category']
    list_display = ('title', 'category', 'url', 'date')


admin.site.register(Category)
admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile)

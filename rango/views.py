__author__ = 'luisfernando'

from django.http import HttpResponse, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .forms import *
from .models import *


def index(request):
    context = RequestContext(request)

    category_list = Category.objects.filter(start_page=True)[:100]
    context_dict = {'categories': category_list}
    most_viewed = Page.objects.filter(views__gt=0).order_by('-views')[:5]
    last_added = Page.objects.order_by('-date')[:5]
    context_dict['most_viewed'] = most_viewed
    context_dict['last_added'] = last_added
    return render_to_response('index.html', context_dict, context)


def about(request):
    context = RequestContext(request)
    context_dict = {}
    return render_to_response('about.html', context_dict, context)


def category(request, category_name_url):
    context = RequestContext(request)
    category_name = Category.url_decode(category_name_url)
    context_dict = {'category_name': category_name}

    try:
        category_obj = Category.objects.get(name=category_name)
        pages = Page.objects.filter(category=category_obj)

        context_dict['pages'] = pages
        context_dict['category'] = category_obj

    except Category.DoesNotExist:
        pass

    return render_to_response('category.html', context_dict, context)


def visit_page(request, page):
    context = RequestContext(request)
    try:
        p = Page.objects.get(id=page)
        p.views += 1
        p.save()
        return redirect(p.url)
    except Page.DoesNotExist:
        return Http404('Page does not Exist')


@login_required
def add_category(request):
    context = RequestContext(request)

    if request.method == 'POST':
        form = CategoryForm(request.POST)

        if form.is_valid():
            category_obj = form.save(commit=True)
            return redirect(category, Category.url_encode(category_obj.name))

        else:
            return HttpResponse('%s %s' % (form.errors, form.__dict__,))
    else:
        form = CategoryForm()

    return render_to_response('add_category.html', {'form': form}, context)


@login_required
def add_page(request, category_name_url):
    context = RequestContext(request)

    category_name = Category.url_decode(category_name_url)
    context_dict = {}

    try:
        cat = Category.objects.get(name=category_name)

    except Category.DoesNotExist:
        return render_to_response('add_category.html', {}, context)

    if request.method == 'POST':
        form = PageForm(request.POST)

        if form.is_valid():
            page = form.save(commit=False)
            page.category = cat
            page.views = 0
            page.save()
            return redirect(category, category_name_url)
        else:
            pass
    else:
        form = PageForm()

    context_dict['category_name_url'] = category_name_url
    context_dict['category_name'] = category_name
    context_dict['category'] = cat
    context_dict['form'] = form

    return render_to_response('add_page.html', context_dict, context)


def register(request):
    context = RequestContext(request)

    registered = False

    if request.method == 'POST':
        user_form = UserForm(request.POST)
        userprofile_form = UserProfileForm(request.POST)

        if user_form.is_valid() and userprofile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = userprofile_form.save(commit=False)
            profile.user = user
            profile.picture = request.FILES.get('picture')
            profile.save()
            registered = True

        else:
            print user_form.errors, userprofile_form.errors

    else:
        user_form = UserForm()
        userprofile_form = UserProfileForm()

    register_forms = (user_form, userprofile_form,)
    return render_to_response('register.html', {'forms': register_forms, 'registered': registered}, context)


def user_login(request):
    context = RequestContext(request)
    if request.user.is_authenticated():
        return redirect(index)

    if request.method == 'POST':
        login_form = LoginForm(request.POST)

        if login_form.is_valid():
            username = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user:
                if user.is_active:
                    login(request, user)
                    return redirect(index)
                else:
                    return HttpResponse("Disabled account")
            else:
                return HttpResponse("Invalid login")
    else:
        login_form = LoginForm()

    return render_to_response('login.html', {'forms': [login_form]}, context)


@login_required
def user_logout(request):
    logout(request)
    return redirect(index)


@login_required
def test_login(request):
    return HttpResponse("Logged in")